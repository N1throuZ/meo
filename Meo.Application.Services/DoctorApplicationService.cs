﻿using System;
using Meo.DataAccess;
using Meo.Domain.Models;
using Meo.Domain.Services.Factories;
using Meo.Infrastructure;
using Meo.Infrastructure.Assertions;

namespace Meo.Application.Services
{
    public class DoctorApplicationService : IDoctorApplicationService
    {
        private readonly DoctorFactory _doctorFactory;
        private readonly IConscriptCheckListRepository _conscriptCheckListRepository;
        private readonly IAliveReportRepository _aliveReportRepository;

        public DoctorApplicationService(
            DoctorFactory doctorFactory,
            IConscriptCheckListRepository conscriptCheckListRepository,
            IAliveReportRepository aliveReportRepository,
            IMapper mapper)
        {
            _doctorFactory = doctorFactory;
            _conscriptCheckListRepository = conscriptCheckListRepository;
            _aliveReportRepository = aliveReportRepository;
        }

        public void ObtainConscriptMedicalClearence(
            string conscriptPassportIdentityNumber,
            bool conscriptAlive)
        {
            AssertionConcern.AssertThatArgumentIsNotNull(conscriptPassportIdentityNumber);

            var checklist = _conscriptCheckListRepository.GetByPassportIdentityNumber(conscriptPassportIdentityNumber);
            if (!checklist.PassportOfficerPassed)
            {
                throw new InvalidOperationException();
            }

            var doctor = _doctorFactory.GetDoctor();
            var aliveReport = doctor.CreateAliveReport(
                conscriptPassportIdentityNumber,
                conscriptAlive);
            _aliveReportRepository.Create(aliveReport);

            checklist = doctor.MarkPassedForConscript(checklist);

            _conscriptCheckListRepository.Update(checklist);
        }
    }
}