using System;
using Meo.DataAccess;
using Meo.Domain.Models;
using Meo.Domain.Services.Factories;
using Meo.Infrastructure;

namespace Meo.Application.Services
{
    public class CommissarApplicationService : ICommissarApplicationService
    {
        private readonly CommissarFactory _commissarFactory;
        private readonly IConscriptCheckListRepository _conscriptCheckListRepository;
        private readonly IPassportRepository _passportRepository;
        private readonly IAliveReportRepository _aliveReportRepository;
        private readonly IMilitaryProfileRepository _militaryProfileRepository;

        public CommissarApplicationService(
            CommissarFactory commissarFactory,
            IConscriptCheckListRepository conscriptCheckListRepository,
            IPassportRepository passportRepository,
            IAliveReportRepository aliveReportRepository,
            IMilitaryProfileRepository militaryProfileRepository)
        {
            _commissarFactory = commissarFactory;
            _conscriptCheckListRepository = conscriptCheckListRepository;
            _passportRepository = passportRepository;
            _aliveReportRepository = aliveReportRepository;
            _militaryProfileRepository = militaryProfileRepository;
        }

        public void MakeDecision(
            string conscriptPassportIdentityNumber,
            MilitaryStatus militaryStatus)
        {
            var checkList = _conscriptCheckListRepository
                .GetByPassportIdentityNumber(conscriptPassportIdentityNumber);

            if (!checkList.DoctorPassed || !checkList.PassportOfficerPassed)
            {
                throw new InvalidOperationException("Passport officer or doctor were not passed");
            }
            
            var commissar = _commissarFactory.GetCommissar();
            var passport = _passportRepository.GetByIdentityNumber(conscriptPassportIdentityNumber);
            var aliveReport = _aliveReportRepository.GetByPassportIdentityNumber(conscriptPassportIdentityNumber);

            var militaryProfile = commissar.MakeDecision(passport, aliveReport);

            _militaryProfileRepository.Create(militaryProfile);

            var checklist = _conscriptCheckListRepository
                .GetByPassportIdentityNumber(conscriptPassportIdentityNumber);

            commissar.MarkPassedForConscript(checklist);

            _conscriptCheckListRepository.Update(checklist);
        }
    }
}