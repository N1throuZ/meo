﻿using Meo.Domain.Models;

namespace Meo.Application.Services
{
    public interface ICommissarApplicationService
    {
        void MakeDecision(
            string conscriptPassportIdentityNumber,
            MilitaryStatus militaryStatus);
    }
}