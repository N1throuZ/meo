﻿using System;
using Meo.Domain.Models;

namespace Meo.Application.Services
{
    public interface IPassportOfficerApplicationService
    {
        void IdentifyConscriptWithPassport(
            string passportIdentityNumber,
            string firstName,
            string lastName,
            DateTime dateOfBirth);

        Passport IdentifyConscriptWithoutPassport(
            string firstName,
            string lastName,
            DateTime dateOfBirth);

        void MarkConscriptAsPassed(string passportIdentityNumber);
    }
}