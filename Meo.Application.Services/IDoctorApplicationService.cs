﻿namespace Meo.Application.Services
{
    public interface IDoctorApplicationService
    {
        void ObtainConscriptMedicalClearence(
            string conscriptPassportIdentityNumber,
            bool conscriptAlive);
    }
}