﻿using System;
using Meo.DataAccess;
using Meo.Domain.Models;
using Meo.Domain.Services;
using Meo.Domain.Services.Factories;
using Meo.Infrastructure;
using Meo.Infrastructure.Assertions;

namespace Meo.Application.Services
{
    public class PassportOfficerApplicationService : IPassportOfficerApplicationService
    {
        private readonly PassportOfficerFactory _passportOfficerFactory;
        private readonly IConscriptCheckListRepository _conscriptCheckListRepository;
        private readonly IConscriptRepository _conscriptRepository;
        private readonly IPassportGeneratorService _passportGeneratorService;
        private readonly IPassportRepository _passportRepository;

        public PassportOfficerApplicationService(
            PassportOfficerFactory passportOfficerFactory,
            IConscriptCheckListRepository conscriptCheckListRepository,
            IConscriptRepository conscriptRepository,
            IPassportGeneratorService passportGeneratorService,
            IPassportRepository passportRepository)
        {
            _passportOfficerFactory = passportOfficerFactory;
            _conscriptCheckListRepository = conscriptCheckListRepository;
            _conscriptRepository = conscriptRepository;
            _passportGeneratorService = passportGeneratorService;
            _passportRepository = passportRepository;
        }

        public void IdentifyConscriptWithPassport(
            string passportIdentityNumber,
            string firstName,
            string lastName,
            DateTime dateOfBirth)
        {
            var passportOfficer = _passportOfficerFactory.Get();

            var checkList = passportOfficer.CreateConscriptCheckList(passportIdentityNumber);

            _conscriptCheckListRepository.Create(checkList);

            var conscript = passportOfficer.IdentifyConscript(passportIdentityNumber);
            _conscriptRepository.Create(conscript);
        }

        public Passport IdentifyConscriptWithoutPassport(
            string firstName, 
            string lastName,
            DateTime dateOfBirth)
        {
            var passport = _passportGeneratorService.GeneratePassport(
                firstName,
                lastName,
                dateOfBirth);
            _passportRepository.Create(passport);

            var passportOfficer = _passportOfficerFactory.Get();

            var conscript = passportOfficer.IdentifyConscript(
                passport.IdentityNumber);
            _conscriptRepository.Create(conscript);

            var checkList = passportOfficer.CreateConscriptCheckList(passport.IdentityNumber);
            _conscriptCheckListRepository.Create(checkList);

            return passport;
        }

        public void MarkConscriptAsPassed(string passportIdentityNumber)
        {
            AssertionConcern.AssertThatArgumentIsNotNull(passportIdentityNumber);

            var conscriptCheckList = _conscriptCheckListRepository.GetByPassportIdentityNumber(passportIdentityNumber);

            var passportOfficer = _passportOfficerFactory.Get();
            conscriptCheckList = passportOfficer.MarkPassedForConscript(conscriptCheckList);

            _conscriptCheckListRepository.Update(conscriptCheckList);
        }
    }
}