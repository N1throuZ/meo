﻿using System;

namespace Meo.Infrastructure.Assertions
{
    public static class AssertionConcern
    {
        public static void AssertThatArgumentIsNotNull(object argument)
        {
            if (argument == null)
            {
                throw new ArgumentNullException();
            }
        }

        public static void AssertThatEquals(object first, object second)
        {
            if (first != second)
            {
                throw new InvalidOperationException();
            }
        }
    }
}
