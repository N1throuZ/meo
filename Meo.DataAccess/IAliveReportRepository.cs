﻿using Meo.Domain.Models;

namespace Meo.DataAccess
{
    public interface IAliveReportRepository
    {
        string Create(AliveReport aliveReport);

        AliveReport GetByPassportIdentityNumber(string passportIdentityNumber);
    }
}