﻿using Meo.Domain.Models;

namespace Meo.DataAccess
{
    public interface IPassportRepository
    {
        string Create(Passport passport);

        Passport GetByIdentityNumber(string identityNumber);
    }
}