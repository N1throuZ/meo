﻿using Meo.Domain.Models;

namespace Meo.DataAccess
{
    public interface IMilitaryProfileRepository
    {
        void Create(MilitaryProfile militaryProfile);
    }
}