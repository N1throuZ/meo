﻿using Meo.Domain.Models;

namespace Meo.DataAccess
{
    public interface IConscriptCheckListRepository
    {
        void Create(ConscriptChecklist checkList);

        void Update(ConscriptChecklist checkList);

        ConscriptChecklist GetByPassportIdentityNumber(string passportIdentityNumber);
    }
}