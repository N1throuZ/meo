﻿using Meo.Domain.Models;

namespace Meo.DataAccess
{
    public interface IConscriptRepository
    {
        string Create(Conscript dataModel);
    }
}