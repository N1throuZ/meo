﻿using System;
using Meo.Infrastructure.Assertions;

namespace Meo.Domain.Models
{
    public class Doctor
    {
        [Obsolete("Use factory")]
        public Doctor()
        {
            
        }

        public AliveReport CreateAliveReport(
            string conscriptPassportIdentityNumber,
            bool conscriptAlive)
        {
            AssertionConcern.AssertThatArgumentIsNotNull(conscriptPassportIdentityNumber);

            return new AliveReport(
                conscriptPassportIdentityNumber,
                conscriptAlive);
        }

        public ConscriptChecklist MarkPassedForConscript(
            ConscriptChecklist checklist)
        {
            checklist.MarkDoctorPassed();
            return checklist;
        }
    }
}
