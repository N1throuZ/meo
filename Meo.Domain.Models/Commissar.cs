﻿using System;

namespace Meo.Domain.Models
{
    public class Commissar
    {
        [Obsolete("Use factory")]
        public Commissar()
        {
            
        }

        public MilitaryProfile MakeDecision(
            Passport passport,
            AliveReport aliveReport)
        {
            bool canBecomeMilitary = passport.GetAge() >= 18 && aliveReport.Alive;

            var militaryProfile = new MilitaryProfile(
                passport.IdentityNumber,
                canBecomeMilitary ? MilitaryStatus.Military : MilitaryStatus.Civilian);

            return militaryProfile;
        }

        public ConscriptChecklist MarkPassedForConscript(ConscriptChecklist checklist)
        {
            checklist.MarkCommissarPassed();
            return checklist;
        }
    }
}