﻿using System;
using Meo.Infrastructure.Assertions;

namespace Meo.Domain.Models
{
    public class PassportOfficer
    {
        [Obsolete("Use factory")]
        public PassportOfficer() { }

        public Passport GetConscriptPassportData(
            string passportIdentityId,
            string firstName,
            string lastName,
            DateTime dateOfBirth)
        {
            AssertionConcern.AssertThatArgumentIsNotNull(passportIdentityId);
            AssertionConcern.AssertThatArgumentIsNotNull(firstName);
            AssertionConcern.AssertThatArgumentIsNotNull(lastName);
            AssertionConcern.AssertThatArgumentIsNotNull(dateOfBirth);

            return new Passport(
                passportIdentityId,
                firstName,
                lastName,
                dateOfBirth);
        }

        public ConscriptChecklist CreateConscriptCheckList(
            string conscriptPassportIdentityNumber)
        {
            return new ConscriptChecklist(
                conscriptPassportIdentityNumber,
                false,
                false,
                false);
        }

        public Conscript IdentifyConscript(
            string passportIdentityNumber)
        {

            var conscript = new Conscript(
                passportIdentityNumber,
                null,
                passportIdentityNumber);

            return conscript;
        }

        public ConscriptChecklist MarkPassedForConscript(
            ConscriptChecklist checkList)
        {
            checkList.MarkPassportOfficerPassed();
            return checkList;
        }
    }
}
