﻿namespace Meo.Domain.Models
{
    public enum MilitaryStatus
    {
        Civilian,
        Military
    }
}