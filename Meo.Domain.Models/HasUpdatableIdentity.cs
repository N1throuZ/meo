﻿namespace Meo.Domain.Models
{
    public abstract class HasUpdatableIdentity<TIdentity>
    {
        public abstract void UpdateIdentity(TIdentity identity);
    }
}
