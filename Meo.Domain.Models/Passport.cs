﻿using System;

namespace Meo.Domain.Models
{
    public class Passport
    {
        public Passport(
            string identityNumber,
            string firstName,
            string lastName,
            DateTime dateOfBirth)
        {
            IdentityNumber = identityNumber;
            FirstName = firstName;
            LastName = lastName;
            DateOfBirth = dateOfBirth;
        }

        public string IdentityNumber { get; private set; }

        public string FirstName { get; private set; }

        public string LastName { get; private set; }

        public DateTime DateOfBirth { get; private set; }

        public int GetAge()
        {
            int age = DateTime.Now.Year - DateOfBirth.Year;

            if (age < 0)
            {
                throw new InvalidOperationException();
            }

            return age;
        }
    }
}
