﻿namespace Meo.Domain.Models
{
    public class AliveReport
    {
        public AliveReport(
            string passportIdentityNumber,
            bool alive)
        {
            PassportIdentityNumber = passportIdentityNumber;
            Alive = alive;
        }

        public string PassportIdentityNumber { get; private set; }

        public bool Alive { get; private set; }
    }
}
