﻿using System;

namespace Meo.Domain.Models
{
    public class MilitaryProfile
    {
        public MilitaryProfile(
            string conscriptPassportIdentityNumber,
            MilitaryStatus militaryStatus)
        {
            ConscriptPassportIdentityNumber = conscriptPassportIdentityNumber;
            MilitaryStatus = militaryStatus;
        }

        public string ConscriptPassportIdentityNumber { get; private set; }

        public MilitaryStatus MilitaryStatus { get; private set; }
    }
}
