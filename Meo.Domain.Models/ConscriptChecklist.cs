﻿namespace Meo.Domain.Models
{
    public class ConscriptChecklist
    {
        public ConscriptChecklist(
            string conscriptPassportIdentityNumber,
            bool passportOfficerPassed,
            bool doctorPassed,
            bool commissarPassed)
        {
            ConscriptPassportId = conscriptPassportIdentityNumber;
            PassportOfficerPassed = passportOfficerPassed;
            DoctorPassed = doctorPassed;
            CommissarPassed = commissarPassed;
        }

        public string ConscriptPassportId { get; private set; }

        public bool PassportOfficerPassed { get; private set; }

        public bool DoctorPassed { get; private set; }

        public bool CommissarPassed { get; private set; }

        public void MarkPassportOfficerPassed()
        {
            PassportOfficerPassed = true;
        }

        public void MarkDoctorPassed()
        {
            DoctorPassed = true;
        }

        public void MarkCommissarPassed()
        {
            CommissarPassed = true;
        }
    }
}
