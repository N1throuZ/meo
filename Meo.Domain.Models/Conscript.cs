﻿namespace Meo.Domain.Models
{
    public class Conscript
    {
        public Conscript(
            string passportIdentityNumber,
            int? aliveReportId,
            string conscriptChecklistId)
        {
            PassportIdentityNumber = passportIdentityNumber;
            AliveReportId = aliveReportId;
            ConscriptChecklistId = conscriptChecklistId;
        }

        public string PassportIdentityNumber { get; private set; }

        public int? AliveReportId { get; private set; }

        public string ConscriptChecklistId { get; private set; }

    }
}