﻿using System;
using Meo.Domain.Models;

namespace Meo.Domain.Services
{
    public interface IPassportGeneratorService
    {
        Passport GeneratePassport(
            string firstName,
            string lastName,
            DateTime dateOfBirth);
    }
}