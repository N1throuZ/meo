﻿using System;
using Meo.Domain.Models;

namespace Meo.Domain.Services
{
    public class PassportGeneratorService : IPassportGeneratorService
    {
        public Passport GeneratePassport(
            string firstName, 
            string lastName,
            DateTime dateOfBirth)
        {
            return new Passport(
                Guid.NewGuid().ToString(),
                firstName,
                lastName,
                dateOfBirth);
        }
    }
}