﻿using Meo.Domain.Models;

namespace Meo.Domain.Services.Factories
{
    public class DoctorFactory
    {
        public Doctor GetDoctor()
        {
            return new Doctor();
        }
    }
}