﻿using Meo.Domain.Models;

namespace Meo.Domain.Services.Factories
{
    public class PassportOfficerFactory
    {
        public PassportOfficer Get()
        {
            return new PassportOfficer();
        }
    }
}