﻿using Meo.Domain.Models;

namespace Meo.Domain.Services.Factories
{
    public class CommissarFactory
    {
        public Commissar GetCommissar()
        {
            return new Commissar();
        }
    }
}